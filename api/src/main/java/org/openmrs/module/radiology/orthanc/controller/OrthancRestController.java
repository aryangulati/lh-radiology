/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.controller;

import java.io.IOException;

import org.openmrs.module.radiology.orthanc.exception.OrthancException;
import org.openmrs.module.radiology.orthanc.service.OrthancService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for Orthanc Rest Services.
 */
@RestController
@RequestMapping("/rest/v1" + OrthancRestController.ORTHANC_REST_NAMESPACE)
public class OrthancRestController {

	public static final String ORTHANC_REST_NAMESPACE = "/orthanc";

	@Autowired
	protected OrthancService orthancService;

	@RequestMapping(value = "/health-check", method = RequestMethod.POST)
	public Object healthCheck() {
		return "OK";
	}

	@RequestMapping(value = "/upload/{identifier}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object attachFileToStudy(@PathVariable("identifier") String identifier, @RequestParam("tags") String tags,
			@RequestParam("file") MultipartFile file) throws OrthancException, IOException {

		// TODO: Create Parent/Study/Series if it doesn't exist

		if (file != null && file.isEmpty()) {
			throw new OrthancException("File to be uploaded is empty");
		}

		return orthancService.uploadFileToOrthanc(identifier, tags, file.getBytes());
	}
}
